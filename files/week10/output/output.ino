#define WHITE   0xFF
#define BLACK   0x00
#define GREY    0x92
#define YELLOW1 0xFD
#define YELLOW2 0xFC
#define ORANGE  0xF0
#define RED     0xE0
#define BROWN   0xB8 
#define GREEN   0xDC 

char pikachu[]={
          GREY,   GREY,    WHITE,   WHITE,   WHITE,   WHITE,   GREY,
  WHITE,  WHITE,  YELLOW1, ORANGE,  WHITE,   WHITE,   WHITE,   ORANGE,
  WHITE,  WHITE,  WHITE,   YELLOW2, YELLOW2, YELLOW2, YELLOW2, ORANGE,
  ORANGE, ORANGE, WHITE,   YELLOW2, BLACK,   YELLOW1, YELLOW1, BLACK,
  ORANGE, ORANGE, WHITE,   RED,     YELLOW1, YELLOW1, YELLOW1, ORANGE,
  WHITE,  BROWN,  WHITE,   YELLOW2, ORANGE,  ORANGE,  ORANGE,  WHITE,
  WHITE,  BROWN,  YELLOW2, ORANGE,  YELLOW2, ORANGE,  YELLOW2, WHITE,
  WHITE,  WHITE,  YELLOW2, ORANGE,  BROWN,   BROWN,   ORANGE,  WHITE,  WHITE
};

char alien[]={
         BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, GREEN,
  BLACK, GREEN, BLACK, BLACK, BLACK, BLACK, GREEN, BLACK,
  BLACK, BLACK, GREEN, GREEN, GREEN, GREEN, BLACK, BLACK,
  BLACK, GREEN, BLACK, GREEN, GREEN, BLACK, GREEN, BLACK,
  BLACK, BLACK, GREEN, GREEN, GREEN, GREEN, BLACK, BLACK,
  GREEN, GREEN, GREEN, GREEN, GREEN, GREEN, GREEN, GREEN,
  GREEN, BLACK, GREEN, BLACK, BLACK, GREEN, BLACK, GREEN,
  GREEN, BLACK, GREEN, GREEN, GREEN, GREEN, BLACK, GREEN, GREEN
};

//Define the SPI Pin Numbers
#define SLAVESELECT 10//ss
#define DATAOUT     11//MOSI
#define DATAIN      6//MISO 
#define SPICLOCK    13//sck

char color_buffer[64];

//array to clear 1 matrix
char blank[]={
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK,
  BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK, BLACK
};

void set_matrix(){
  digitalWrite(SLAVESELECT, LOW); 
  spi_transfer(0x25);
  spi_transfer(0x02); 
  digitalWrite(SLAVESELECT, HIGH);
}

int p = 0;
int xMAX = 8;
int yMAX = 8;

void clear_buffer(){
  for(int y=0; y<yMAX; y++){
    for(int i=1; i>0; i--){
      for(int x=0; x<xMAX; x++){
        p = (yMAX*y) + (x + (i-1)*(xMAX*yMAX));
        color_buffer[p] = blank[x];
      }
    }
  } 
}

void print_pikachu(){
  for(int i = 0; i < (64); i++){
    color_buffer[i] = pikachu[i];
  }
}

void setup(){  
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1);
  pinMode(DATAOUT, OUTPUT);
  pinMode(DATAIN, INPUT);
  pinMode(SPICLOCK,OUTPUT);
  pinMode(SLAVESELECT,OUTPUT);
  digitalWrite(SLAVESELECT,HIGH); 
  
  Serial.begin(9600);
  Serial.println("Initializing Matrix");
}

void loop(){
  clear_buffer();
  print_pikachu();
  matrix_write();
  delay(2000);
}

void matrix_write(){
  digitalWrite(SLAVESELECT, LOW);
  spi_transfer(0x26);
  digitalWrite(SLAVESELECT, HIGH);
  digitalWrite(SLAVESELECT, LOW);
  delayMicroseconds(50);

  for(int LED=0; LED<(64); LED++){
    spi_transfer(color_buffer[LED]);
  }
  delayMicroseconds(500);
  digitalWrite(SLAVESELECT, HIGH);
}

char spi_transfer(volatile char data){
  SPDR = data;                  
  while (!(SPSR & (1<<SPIF))){     
  };
  return SPDR;  
}
