#include <Time.h>

//Variablen
// it is      half ten quarter twenty five minutes to past      two three one four five six seven eight nine ten eleven twelve o'clock
// 0          1    2   3       4      5    6       7  8         9   10    11  12   13   14  15    16    17   18  19     20     21
int leds[] = {10, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 31, 30, 29, 28, 27, 26, 25, 24, 0};

boolean allon[] = {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true};
boolean alloff[] = {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
boolean itis[] = {true};

boolean onoff[22] = alloff;

boolean fuenf[] = {false, false, false, false, true, true, false, true};
boolean zehn[] = {false, true, false, false, false, true, false, true};
boolean viertel[] = {false, false, true, false, false, false, false, true};
boolean zwanzig[] = {false, false, false, true, false, true, false, true};
boolean zwanzigfuenf[] = {false, false, false, true, true, true, false, true};
boolean halb[] = {true, false, false, false, false, false, false, true};
boolean dreissigfuenf[] = {false, false, false, true, true, true, true, false};
boolean vierzig[] = {false, false, false, true, false, true, true, false};
boolean dreiviertel[] = {false, false, false, true, true, true, true, false};
boolean fuenfzig[] = {false, true, false, false, false, true, true, false};
boolean fuenzigfuenf[] = {false, false, false, false, true, true, true, false};
boolean nix[] = {false, false, false, false, false, false, false, false};

boolean vollja[] = {true};
boolean vollnein[] = {false};

boolean one[] = {true, false, false, false, false, false, false, false, false, false, false, false};
boolean two[] = {false, true, false, false, false, false, false, false, false, false, false, false};
boolean three[] = {false, false, true, false, false, false, false, false, false, false, false, false};
boolean four[] = {false, false, false, true, false, false, false, false, false, false, false, false};
boolean five[] = {false, false, false, false, true, false, false, false, false, false, false, false};
boolean six[] = {false, false, false, false, false, true, false, false, false, false, false, false};
boolean seven[] = {false, false, false, false, false, false, true, false, false, false, false, false};
boolean eight[] = {false, false, false, false, false, false, false, true, false, false, false, false};
boolean nine[] = {false, false, false, false, false, false, false, false, true, false, false, false};
boolean ten[] = {false, false, false, false, false, false, false, false, false, true, false, false};
boolean eleven[] = {false, false, false, false, false, false, false, false, false, false, true, false};
boolean twelve[] = {false, false, false, false, false, false, false, false, false, false, false, true};

void setup() {
  pinMode(10, OUTPUT); //itis
  pinMode(11, OUTPUT); //half
  pinMode(13, OUTPUT); //ten
  pinMode(14, OUTPUT); //quarter
  pinMode(15, OUTPUT); //twenty
  pinMode(16, OUTPUT); //five
  pinMode(17, OUTPUT); //minutes
  pinMode(18, OUTPUT); //to
  pinMode(19, OUTPUT); //past
  pinMode(20, OUTPUT); //two
  pinMode(21, OUTPUT); //three
  pinMode(22, OUTPUT); //one
  pinMode(23, OUTPUT); //four
  pinMode(31, OUTPUT); //five
  pinMode(30, OUTPUT); //six
  pinMode(29, OUTPUT); //seven
  pinMode(28, OUTPUT); //eight
  pinMode(27, OUTPUT); //nine
  pinMode(26, OUTPUT); //ten
  pinMode(25, OUTPUT); //eleven
  pinMode(24, OUTPUT); //twelve
  pinMode(0, OUTPUT); //o'clock
}

void loop() {
  digitalWrite(13, HIGH);
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);
}

void setTime() {
  getTime();
  for(int i = 0; i < onoff.length; i++){
    if(onoff[i]){
      digitalWrite(leds[i], HIGH);
    }else if(!onoff[i]){
      digitalWrite(leds[i], LOW);
    }
  }
}

void getTime(){
  boolean tmp[8];
  boolean tmptwo[12];
  onoff[0] = true;
  
  //Minuten
  int x = (DateTime.Minute % 60) / 5;
  switch(x){
    case 0:
      onoff[21] = true;
      tmp = nix;
      break;
    case 1:
      onoff[21] = false;
      tmp = fuenf;  
      break;
    case 2:
      onoff[21] = false;
      tmp = zehn;
      break;
    case 3:
      onoff[21] = false;
      tmp = viertel;
      break;
    case 4:
      onoff[21] = false;
      tmp = zwanzig;
      break;
    case 5:
      onoff[21] = false;
      tmp = zwanzigfuenf;
      break;
    case 6:
      onoff[21] = false;
      tmp = halb;
      break;
    case 7:
      onoff[21] = false;
      tmp = dreissigfuenf;
      break;
    case 8:
      onoff[21] = false;
      tmp = vierzig;
      break;
    case 9:
      onoff[21] = false;
      tmp = dreiviertel;
      break;
    case 10:
      onoff[21] = false;
      tmp = fuenfzig;
      break;
    case 11:
      onoff[21] = false;
      tmp = fuenfzigfuenf;
      break;
    default:
      onoff = allon;
      break;
  }
  for(int i = 1; i < 9; i++){
    onoff[i] = tmp[i-1];
  }
  //Stunden
  int y = /DateTime.Hour % 12)
  switch(y){
    case 0:
      tmptwo = twelve;
      break;
    case 1:
      tmptwo = one; 
      break;
    case 2:
      tmptwo = two;
      break;
    case 3:
      tmptwo = three;
      break;
    case 4:
      tmptwo = four;
      break;
    case 5:
      tmptwo = five;
      break;
    case 6:
      tmptwo = six;
      break;
    case 7:
      tmptwo = seven;
      break;
    case 8:
      tmptwo = eight;
      break;
    case 9:
      tmptwo = nine;
      break;
    case 10:
      tmptwo = ten;
      break;
    case 11:
      tmptwo = eleven;
      break;
    default:
      onoff = allon;
      break;
  }
  for(int i = 9; i < 21; i++){
    onoff[i] = tmp[i-9];
  }
  //Console Log
  for(int i = 0; i < onoff.length; i++){
    Serial.println(onoff[i]);
  }
}

void setArray(x[], y[]){
  for(int i = 0; i < x.length; i++){
    x[i] = y[i];
  }
}
