#define LED_ONE 13
#define LED_TWO 11
#define BUTTON 10

boolean pressed_once = false;
boolean pressed_twice = false;
    
void setup() {
  Serial.begin(9600);
  
  pinMode(LED_ONE, OUTPUT);
  pinMode(LED_TWO, OUTPUT);
  pinMode(BUTTON, INPUT);
  digitalWrite(BUTTON, HIGH);

  Serial.println("Program started"); 
}

void loop() {

  //int brightness = map(photocellReadiing, 0, 1023, 0, 255);

  if(!digitalRead(BUTTON)){
    Serial.println("pressed"); 
    if(!pressed_once){
      pressed_once = true;
      Serial.println("1 LED on");
    }else if(!pressed_twice && pressed_once){
      pressed_twice = true;
      Serial.println("2 LEDs on");
    }else{
      pressed_once = false;
      pressed_twice = false;
      Serial.println("0 LEDs on");
    }
  }
  
  if(pressed_twice){
    digitalWrite(LED_ONE, HIGH);
    digitalWrite(LED_TWO, HIGH);    
  }else if(pressed_once){
    digitalWrite(LED_ONE, HIGH); 
  }else{
    digitalWrite(LED_ONE, LOW);
    digitalWrite(LED_TWO, LOW);
  }
  delay(250);
}
