int leds[] = {4,5,7,3,6,0,15,2,29,30,31,26,27,28,23,24,25,20,21,22,18,19};
//            1,2,3,4,5,6, 7,8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22
void setup() {
  for(int i=0; i<23; i++){
    pinMode(leds[i], OUTPUT);
  }
}

void loop(){
  delay(1000);
  digitalWrite(15, HIGH);
  delay(5000);
  digitalWrite(15, LOW);
  for(int i=16; i<22; i++){
    digitalWrite(leds[i], HIGH);
    delay(750);
    digitalWrite(leds[i], LOW);
    delay(750);
  }
  delay(750);
}
