//RTC
#include <Wire.h>
#include "RTClib.h"
RTC_DS3231 rtc;
DateTime now;

//LED-Matrix
int leds[] = {4,5,7,3,6,0,15,2,29,30,31,26,27,28,23,24,25,20,21,22,18,19};
boolean timematrix[22];

//minutes
boolean zeromin[] = {false, false, false, false, false, false, false, false};
boolean fivemin[] = {false, false, false, false, true, true, false, true};
boolean tenmin[] = {false, true, false, false, false, true, false, true};
boolean fifteenmin[] = {false, false, true, false, false, false, false, true};
boolean twentymin[] = {false, false, false, true, false, true, false, true};
boolean twentyfivemin[] = {false, false, false, true, true, true, false, true};
boolean thirtymin[] = {true, false, false, false, false, false, false, true};
boolean thirtyfivemin[] = {false, false, false, true, true, true, true, false};
boolean fourtymin[] = {false, false, false, true, false, true, true, false};
boolean fourtyfivemin[] = {false, false, true, false, false, false, true, false};
boolean fiftymin[] = {false, true, false, false, false, true, true, false};
boolean fiftyfivemin[] = {false, false, false, false, true, true, true, false};

//hours
boolean one[] = {false, false, true, false, false, false, false, false, false, false, false, false};
boolean two[] = {true, false, false, false, false, false, false, false, false, false, false, false};
boolean three[] = {false, true, false, false, false, false, false, false, false, false, false, false};
boolean four[] = {false, false, false, true, false, false, false, false, false, false, false, false};
boolean five[] = {false, false, false, false, true, false, false, false, false, false, false, false};
boolean six[] = {false, false, false, false, false, true, false, false, false, false, false, false};
boolean seven[] = {false, false, false, false, false, false, true, false, false, false, false, false};
boolean eight[] = {false, false, false, false, false, false, false, true, false, false, false, false};
boolean nine[] = {false, false, false, false, false, false, false, false, true, false, false, false};
boolean ten[] = {false, false, false, false, false, false, false, false, false, true, false, false};
boolean eleven[] = {false, false, false, false, false, false, false, false, false, false, true, false};
boolean twelve[] = {false, false, false, false, false, false, false, false, false, false, false, true};

void setup() {
  Wire.begin();
  //rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  Serial.begin(9600);
  now = rtc.now();

  for(int i=0; i<22; i++){
    pinMode(leds[i], OUTPUT);
  }
  
  getMatrix();
  setMatrix();
}

void loop() {
  if(now.minute() != rtc.now().minute()){
    now = rtc.now();
    getMatrix();
    setMatrix();
  }
  delay(5000); 
}

void setMatrix(){
  for(int i = 0; i < 22; i++){
    if(timematrix[i]){
      digitalWrite(leds[i], HIGH);
    }else{
      digitalWrite(leds[i], LOW);
    }
  }
}

void getMatrix(){
  
  //refresh time
  now = rtc.now();  
  int m = now.minute();
  int h = now.hour();
  
  //set it is
  timematrix[0] = true;
  //oclock
  timematrix[21] = false;
  
  //set minutes
  if(0<=m & m<=4){
    setMinutes(zeromin);
    timematrix[21] = true;
  }else if(5<=m & m<=9){
    setMinutes(fivemin);
  }else if(10<=m & m<=14){
    setMinutes(tenmin);
  }else if(15<=m & m<=19){
    setMinutes(fifteenmin);
  }else if(20<=m & m<=24){
    setMinutes(twentymin);
  }else if(25<=m & m<=29){
    setMinutes(twentyfivemin);
  }else if(30<=m & m<=34){
    setMinutes(thirtymin);
  }else if(35<=m & m<=39){
    setMinutes(thirtyfivemin);
  }else if(40<=m & m<=44){
    setMinutes(fourtymin);
  }else if(45<=m & m<=49){
    setMinutes(fourtyfivemin);
  }else if(50<=m & m<=54){
    setMinutes(fiftymin);
  }else if(55<=m & m<=59){
    setMinutes(fiftyfivemin);
  }
  
  //set hours
  if( ( (h == 1 | h == 13) & m < 35) | ( (h == 0 | h == 12) & m >= 35) ){
    setHours(one);
  }else if( ( (h == 2 | h == 14) & m < 35) | ( (h == 1 | h == 13) & m >= 35) ){
    setHours(two);  
  }else if( ( (h == 3 | h == 15) & m < 35) | ( (h == 2 | h == 14) & m >= 35) ){
    setHours(three);  
  }else if( ( (h == 4 | h == 16) & m < 35) | ( (h == 3 | h == 15) & m >= 35) ){
    setHours(four);  
  }else if( ( (h == 5 | h == 17) & m < 35) | ( (h == 4 | h == 16) & m >= 35) ){
    setHours(five);  
  }else if( ( (h == 6 | h == 18) & m < 35) | ( (h == 5 | h == 17) & m >= 35) ){
    setHours(six);  
  }else if( ( (h == 7 | h == 19) & m < 35) | ( (h == 6 | h == 18) & m >= 35) ){
    setHours(seven);  
  }else if( ( (h == 8 | h == 20) & m < 35) | ( (h == 7 | h == 19) & m >= 35) ){
    setHours(eight);  
  }else if( ( (h == 9 | h == 21) & m < 35) | ( (h == 8 | h == 20) & m >= 35) ){
    setHours(nine);  
  }else if( ( (h == 10 | h == 22) & m < 35) | ( (h == 9 | h == 21) & m >= 35) ){
    setHours(ten);  
  }else if( ( (h == 11 | h == 23) & m < 35) | ( (h == 10 | h == 22) & m >= 35) ){
    setHours(eleven);  
  }else if( ( (h == 0 | h == 12) & m < 35) | ( (h == 11 | h == 23) & m >= 35) ){
    setHours(twelve);  
  }
}

void setMinutes(boolean x[]){
  for(int i=0; i<8; i++){
      timematrix[i+1] = x[i];
  }
}

void setHours(boolean x[]){
  for(int i=0; i<12; i++){
      timematrix[i+9] = x[i];
  }
}
