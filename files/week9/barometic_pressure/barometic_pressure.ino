#define TRIGGER 19
#define ECHO 18

int duration = 0;
float distance = 0;

void setup() {
  Serial.begin(115200);
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);  

  Serial.println("Program started"); 
}

void loop() {
   
  digitalWrite(TRIGGER, LOW);
  delay(5);
  digitalWrite(TRIGGER, HIGH);
  delay(10);
  digitalWrite(TRIGGER, LOW);
  duration = pulseIn(ECHO, HIGH);
  distance = (duration/2) * 0.03432;
  if (distance >= 500 || distance <= 0){
    Serial.println("No measurement");
  }else{
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println("cm");
  }
  delay(2000);
}
